// import logo from './logo.svg';
// import './App.css';
// import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
// /*
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

// */


// employeeService = (function () {

//   var findById = function (id) {
//           var deferred = $.Deferred();
//           var employee = null;
//           var l = employees.length;
//           for (var i = 0; i < l; i++) {
//               if (employees[i].id == id) {
//                   employee = employees[i];
//                   break;
//               }
//           }
//           deferred.resolve(employee);
//           return deferred.promise();
//       },

//       findByName = function (searchKey) {
//           var deferred = $.Deferred();
//           var results = employees.filter(function (element) {
//               var fullName = element.firstName + " " + element.lastName;
//               return fullName.toLowerCase().indexOf(searchKey.toLowerCase()) > -1;
//           });
//           deferred.resolve(results);
//           return deferred.promise();
//       },

//       findByManager = function (managerId) {
//           var deferred = $.Deferred();
//           var results = employees.filter(function (element) {
//               return managerId === element.managerId;
//           });
//           deferred.resolve(results);
//           return deferred.promise();
//       },

//       employees = [
//           {"id": 1, "firstName": "James", "lastName": "King", "managerId": 0, "managerName": "", "reports": 4, "title": "President and CEO", "department": "Corporate", "mobilePhone": "617-000-0001", "officePhone": "781-000-0001", "email": "jking@fakemail.com", "city": "Boston, MA", "pic": "james_king.jpg", "twitterId": "@fakejking", "blog": "http://coenraets.org"},
//           {"id": 2, "firstName": "Julie", "lastName": "Taylor", "managerId": 1, "managerName": "James King", "reports": 2, "title": "VP of Marketing", "department": "Marketing", "mobilePhone": "617-000-0002", "officePhone": "781-000-0002", "email": "jtaylor@fakemail.com", "city": "Boston, MA", "pic": "julie_taylor.jpg", "twitterId": "@fakejtaylor", "blog": "http://coenraets.org"},
//           {"id": 3, "firstName": "Eugene", "lastName": "Lee", "managerId": 1, "managerName": "James King", "reports": 0, "title": "CFO", "department": "Accounting", "mobilePhone": "617-000-0003", "officePhone": "781-000-0003", "email": "elee@fakemail.com", "city": "Boston, MA", "pic": "eugene_lee.jpg", "twitterId": "@fakeelee", "blog": "http://coenraets.org"},
//           {"id": 4, "firstName": "John", "lastName": "Williams", "managerId": 1, "managerName": "James King", "reports": 3, "title": "VP of Engineering", "department": "Engineering", "mobilePhone": "617-000-0004", "officePhone": "781-000-0004", "email": "jwilliams@fakemail.com", "city": "Boston, MA", "pic": "john_williams.jpg", "twitterId": "@fakejwilliams", "blog": "http://coenraets.org"},
//           {"id": 5, "firstName": "Ray", "lastName": "Moore", "managerId": 1, "managerName": "James King", "reports": 2, "title": "VP of Sales", "department": "Sales", "mobilePhone": "617-000-0005", "officePhone": "781-000-0005", "email": "rmoore@fakemail.com", "city": "Boston, MA", "pic": "ray_moore.jpg", "twitterId": "@fakermoore", "blog": "http://coenraets.org"},
//           {"id": 6, "firstName": "Paul", "lastName": "Jones", "managerId": 4, "managerName": "John Williams", "reports": 0, "title": "QA Manager", "department": "Engineering", "mobilePhone": "617-000-0006", "officePhone": "781-000-0006", "email": "pjones@fakemail.com", "city": "Boston, MA", "pic": "paul_jones.jpg", "twitterId": "@fakepjones", "blog": "http://coenraets.org"},
//           {"id": 7, "firstName": "Paula", "lastName": "Gates", "managerId": 4, "managerName": "John Williams", "reports": 0, "title": "Software Architect", "department": "Engineering", "mobilePhone": "617-000-0007", "officePhone": "781-000-0007", "email": "pgates@fakemail.com", "city": "Boston, MA", "pic": "paula_gates.jpg", "twitterId": "@fakepgates", "blog": "http://coenraets.org"},
//           {"id": 8, "firstName": "Lisa", "lastName": "Wong", "managerId": 2, "managerName": "Julie Taylor", "reports": 0, "title": "Marketing Manager", "department": "Marketing", "mobilePhone": "617-000-0008", "officePhone": "781-000-0008", "email": "lwong@fakemail.com", "city": "Boston, MA", "pic": "lisa_wong.jpg", "twitterId": "@fakelwong", "blog": "http://coenraets.org"},
//           {"id": 9, "firstName": "Gary", "lastName": "Donovan", "managerId": 2, "managerName": "Julie Taylor", "reports": 0, "title": "Marketing Manager", "department": "Marketing", "mobilePhone": "617-000-0009", "officePhone": "781-000-0009", "email": "gdonovan@fakemail.com", "city": "Boston, MA", "pic": "gary_donovan.jpg", "twitterId": "@fakegdonovan", "blog": "http://coenraets.org"},
//           {"id": 10, "firstName": "Kathleen", "lastName": "Byrne", "managerId": 5, "managerName": "Ray Moore", "reports": 0, "title": "Sales Representative", "department": "Sales", "mobilePhone": "617-000-0010", "officePhone": "781-000-0010", "email": "kbyrne@fakemail.com", "city": "Boston, MA", "pic": "kathleen_byrne.jpg", "twitterId": "@fakekbyrne", "blog": "http://coenraets.org"},
//           {"id": 11, "firstName": "Amy", "lastName": "Jones", "managerId": 5, "managerName": "Ray Moore", "reports": 0, "title": "Sales Representative", "department": "Sales", "mobilePhone": "617-000-0011", "officePhone": "781-000-0011", "email": "ajones@fakemail.com", "city": "Boston, MA", "pic": "amy_jones.jpg", "twitterId": "@fakeajones", "blog": "http://coenraets.org"},
//           {"id": 12, "firstName": "Steven", "lastName": "Wells", "managerId": 4, "managerName": "John Williams", "reports": 0, "title": "Software Architect", "department": "Engineering", "mobilePhone": "617-000-0012", "officePhone": "781-000-0012", "email": "swells@fakemail.com", "city": "Boston, MA", "pic": "steven_wells.jpg", "twitterId": "@fakeswells", "blog": "http://coenraets.org"}
//       ];

//   // The public API
//   return {
//       findById: findById,
//       findByName: findByName,
//       findByManager: findByManager
//   };

// }());




// var Header = React.createClass({
//   render: function () {
//       return (
//           <h1 className="title">{this.props.text}</h1>
//       );
//   }
// });

// var SearchBar = React.createClass({
//   getInitialState: function() {
//       return {searchKey: ""};
//   },
//   searchHandler: function(event) {
//       var searchKey = event.target.value;
//       this.setState({searchKey: searchKey});
//       this.props.searchHandler(searchKey);
//   },
//   render: function () {
//       return (
//           <input type="search" value={this.state.symbol} onChange={this.searchHandler}/>
//       );
//   }
// });

// var EmployeeListItem = React.createClass({
//   render: function () {
//       return (
//           <li>
//               <a href={"#employees/" + this.props.employee.id}>
//                   {this.props.employee.firstName} {this.props.employee.lastName}
//               </a>
//           </li>
//       );
//   }
// });

// var EmployeeList = React.createClass({
//   render: function () {
//       var items = this.props.employees.map(function (employee) {
//           return (
//               <EmployeeListItem key={employee.id} employee={employee} />
//           );
//       });
//       return (
//           <ul>
//               {items}
//           </ul>
//       );
//   }
// });

// var HomePage = React.createClass({
//   getInitialState: function() {
//       return {employees: []}
//   },
//   searchHandler:function(key) {
//       this.props.service.findByName(key).done(function(result) {
//           this.setState({searchKey: key, employees: result});
//       }.bind(this));
//   },
//   render: function () {
//       return (
//           <div>
//               <Header text="Employee Directory"/>
//               <SearchBar searchHandler={this.searchHandler}/>
//               <EmployeeList employees={this.state.employees}/>
//           </div>
//       );
//   }
// });

// /*
// React.render(
//   <HomePage service={employeeService}/>,
//   document.body
// );*/


// function App() {
//   return (
//     <div className="App">
//       <HomePage service={employeeService}/>
//     </div>
//   );
// }

// export default App;