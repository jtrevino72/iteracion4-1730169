

var Header = React.createClass({
    render: function () {
        return (
            <h2 className="title">{this.props.text}</h2>
        );
    }
});

var SearchBar = React.createClass({
    getInitialState: function() {
        return {searchKey: ""};
    },
    searchHandler: function(event) {
        var searchKey = event.target.value;
        this.setState({searchKey: searchKey});
        this.props.searchHandler(searchKey);
    },
    render: function () {
        return (
            <input type="search" value={this.state.symbol} onChange={this.searchHandler} className="form-control" placeholder="Escribe para buscar..."  />
        );
    }
});

var EmployeeListItem = React.createClass({
    render: function () {
        return (
            <li className="list-group-item">
                <a href={"#employees/" + this.props.employee.id}>
                    {this.props.employee.firstName} {this.props.employee.lastName}
                </a>
            </li>
        );
    }
});

var EmployeeList = React.createClass({
    render: function () {
        var items = this.props.employees.map(function (employee) {
            return (
                <EmployeeListItem key={employee.id} employee={employee} />
            );
        });
        return (
            <ul className="list-group ">
                {items}
            </ul>
        );
    }
});

var HomePage = React.createClass({
    getInitialState: function() {
        return {employees: []}
    },
    searchHandler:function(key) {
        this.props.service.findByName(key).done(function(result) {
            this.setState({searchKey: key, employees: result});
        }.bind(this));
    },
    render: function () {
        return (
            <div className="container">

                <br></br>
                <div className="card">
                    <div className="card-header bg-primary text-white">
                        <Header text="ITERACION 4 - Employee Directory"/>
                    </div>

                    <div className="card-body">
                        <SearchBar searchHandler={this.searchHandler}/>
                        <br></br>
                        <h4>Lista:</h4>
                        <EmployeeList employees={this.state.employees}/>
                    </div>
                </div>
                
                <br></br>

                
            </div>
        );
    }
});

React.render(
    <HomePage service={employeeService}/>,
    document.body
);